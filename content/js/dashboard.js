/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9249074201069812, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.8683221476510067, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999536034642746, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8875959395890072, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.6191765980498375, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7692016647748771, 500, 1500, "me"], "isController": false}, {"data": [0.955327281414238, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.7764136904761905, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.888121546961326, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.003076923076923077, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 58328, 0, 0.0, 256.93090796872985, 10, 14027, 73.0, 609.0, 954.0, 2018.9800000000032, 192.64470316241434, 617.3183405065644, 235.08512728614485], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 3725, 0, 0.0, 402.31489932885944, 45, 2176, 300.0, 847.8000000000002, 1045.0999999999995, 1420.0, 12.377964896423848, 7.820843054674053, 15.31531398805568], "isController": false}, {"data": ["getLatestMobileVersion", 32330, 0, 0.0, 45.97655428394634, 10, 1101, 26.0, 90.0, 149.0, 236.0, 107.76738511590077, 72.0904871136641, 79.45739820557138], "isController": false}, {"data": ["findAllConfigByCategory", 4039, 0, 0.0, 371.2097053726174, 31, 2379, 274.0, 771.0, 1000.0, 1384.7999999999997, 13.418827555275005, 15.174806942391069, 17.428750633316167], "isController": false}, {"data": ["getNotifications", 1846, 0, 0.0, 811.9777898158188, 86, 2500, 763.0, 1498.0, 1647.6499999999999, 2099.5999999999995, 6.1330941227283295, 52.053438496710854, 6.821869341589421], "isController": false}, {"data": ["getHomefeed", 146, 0, 0.0, 10312.82191780822, 4841, 14027, 10436.0, 11685.9, 11986.0, 13716.33, 0.48220944403893345, 8.433485286419199, 2.412930850835444], "isController": false}, {"data": ["me", 2643, 0, 0.0, 567.7975785092696, 72, 2151, 455.0, 1179.1999999999998, 1349.0, 1610.6799999999998, 8.779185060437863, 11.545695312577852, 27.820757344844594], "isController": false}, {"data": ["getAllClassInfo", 6279, 0, 0.0, 238.1911132345919, 20, 1956, 182.0, 471.0, 649.0, 1002.5999999999995, 20.86129679588555, 12.060437210121334, 53.55893484021788], "isController": false}, {"data": ["findAllChildrenByParent", 2688, 0, 0.0, 558.0970982142859, 67, 2447, 433.0, 1204.3999999999996, 1364.0, 1740.2100000000014, 8.929876549772102, 10.02866995335734, 14.266873081471836], "isController": false}, {"data": ["findAllSchoolConfig", 3982, 0, 0.0, 376.4274234053231, 36, 2258, 285.0, 748.1000000000008, 989.0, 1390.17, 13.227917483307312, 288.4822785519716, 9.701334013636515], "isController": false}, {"data": ["getChildCheckInCheckOut", 650, 0, 0.0, 2310.6646153846104, 1438, 3876, 2259.5, 2880.9, 3016.849999999999, 3410.3100000000004, 2.1553582205362534, 143.15872462065695, 9.918015561686351], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 58328, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
